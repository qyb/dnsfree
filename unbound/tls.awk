{
    if ($0 != "" && $0 !~ /^#/) {
        print "\tdomain-insecure: \""$0".\"";
        c[FNR]=$0
    }
} END {
    for (i = 0; i <= NR; i++)
        if (c[i])
            print "\nforward-zone:\n \tname: \""c[i]".\"\n \tforward-addr: 118.89.110.78@853\n \tforward-addr: 47.96.179.163@853\n \tforward-tls-upstream: yes"
}
